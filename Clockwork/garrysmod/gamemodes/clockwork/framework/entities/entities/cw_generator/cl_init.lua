--[[
	
--]]

include("shared.lua");

-- Called when the entity should draw.
function ENT:Draw()
	if (!Clockwork.entity:HasFetchedItemData(self)) then
		Clockwork.entity:FetchItemData(self);
		return;
	end;
	
	if (Clockwork.plugin:Call("GeneratorEntityDraw", self) != false) then
		self:DrawModel();
	end;
end;