--[[
	
--]]

Clockwork.config:AddToSystem("Remove map physics", "remove_map_physics", "Whether or not physics entities should be removed when the map is loaded.");