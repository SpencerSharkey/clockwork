Clockwork
---------
A roleplaying framework developed by Cloud Sixteen for the people.  
*Clockwork was created and is under development by Conna Wiles, also known as kurozael.*


Installation
------------
http://wiki.cloudsixteen.com/Installing  
http://wiki.cloudsixteen.com/

Donations
------------
If you'd like to support the development you can donate via Bitcoin to the following address
**12wPvfmNkokbyyjNFUcCVHrJiQR4vL76Q1**

Or via PayPal to **kurozael@gmail.com**

Contributing
------------
If you wish to contribute to the development of Clockwork, you may issue a pull request using GitHub. You can find the Clockwork GitHub repository here https://github.com/CloudSixteen/Clockwork/  
  
**We will only accept your pull request if it meets the following requirements:**  
  
* Pull requests must only contain one commit. Add all of your changes files to one commit.
* All contributions must match the coding standard used throughout Clockwork.
* Commit messages must be descriptive, well punctuated, and tidy.
  * You can use `Fixes #<id>` in your title to automatically close an issue by that id. For example (and this formatting is mandatory) `Fixed an awful bug where so and so would happen. Fixes #3.` You could also fix multiple in the commit title, `Fixed a bug where so and so would happen, and also fixed a bug where another thing would occur. Fixes #2, Fixes #3.`
* You agree to contribute your code under the terms of the license below:

License
-------
The MIT License (MIT)

Copyright (c) 2014

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.